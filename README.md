# devops-mariadb2k8

##Setup Google Cloud SDK
###Install on Ubuntu
```
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install google-cloud-sdk kubectl
```
###Install on Centos
```
sudo tee -a /etc/yum.repos.d/google-cloud-sdk.repo << EOM
[google-cloud-sdk]
name=Google Cloud SDK
baseurl=https://packages.cloud.google.com/yum/repos/cloud-sdk-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOM
yum install google-cloud-sdk kubectl
```
###Initialize gcloud and create a project
```
gcloud init
```
###Select default project
```
gcloud projects list
gcloud config set project <PROJECT_ID>
```
##Create GKE cluster
###Enable k8s api
```
gcloud services list --available 
gcloud services enable container.googleapis.com
```
###Select default zone
```
gcloud compute zones list
gcloud config set compute/zone <ZONE_NAME>
```
###Create the cluster
```
gcloud container clusters create <CLUSTER_NAME>
```
###Select the cluster as a default cluster for gcloud
```
gcloud container clusters list
gcloud config set container/cluster <CLUSTER_NAME>
```
###Configure kubectl to use the cluster
```
kubectl config current-context
gcloud container clusters --zone=<ZONE_NAME> get-credentials <CLUSTER_NAME>
```
##Deploy mariadb using kubectl
###Start server
```
kubectl run <SERVER_DEPLOYMENT_NAME> --image mariadb --env "MYSQL_ROOT_PASSWORD=<PASSWORD>"
```
###Get server pod ip and start a client
```
kubectl get pods -o wide
kubectl run <CLIENT_DEPLOYMENT_NAME> -it --rm --image mariadb --command -- mysql -h<SERVER_POD_IP> -uroot -p<PASSWORD>
```